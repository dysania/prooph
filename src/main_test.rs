#[cfg(test)]

use rocket::http::Status;
use rocket::local::Client;
use super::rocket;

#[test]
fn index() {
  let client = Client::new(rocket()).expect("valid rocket instance");
  let response = client.get("/").dispatch();
  assert_eq!(response.status(), Status::Ok);
}

#[test]
fn public_key() {
  let client = Client::new(rocket()).expect("valid rocket instance");
  let response = client.get("/prooph.pub").dispatch();
  assert_eq!(response.status(), Status::Ok);
}

#[test]
fn new_good_prooph() {
  let client = Client::new(rocket()).expect("valid rocket instance");
  let response = client.get("/prooph?hash=cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e").dispatch();
  assert_eq!(response.status(), Status::Ok);
}

#[test]
fn new_prooph_invalid_length() {
  let client = Client::new(rocket()).expect("valid rocket instance");
  let response = client.get("/prooph?hash=8318d2877eec2f63b931bd47417a81a538327af927da3e").dispatch();
  assert_eq!(response.status(), Status::NotFound);
}

#[test]
fn new_prooph_invalid_chars() {
  let client = Client::new(rocket()).expect("valid rocket instance");
  let response = client.get("/prooph?hash=cf83e1357eefb8bdf154%850d66d[007d.20e4050b5*15dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e").dispatch();
  assert_eq!(response.status(), Status::BadRequest);
}
