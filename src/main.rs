#![feature(proc_macro_hygiene, decl_macro)]

extern crate minisign;
extern crate regex;
#[macro_use]
extern crate rocket;
extern crate rocket_contrib;
extern crate serde_derive;

use minisign::SecretKey;

use regex::Regex;
use rocket::http::RawStr;
use rocket::request::FromFormValue;
use rocket::State;
use rocket_contrib::serve::StaticFiles;

mod prooph;
mod main_test;

/// Sha512Hash query guard.
struct Sha512Hash(String);

impl<'v> FromFormValue<'v> for Sha512Hash {
  type Error = &'v RawStr;

  fn from_form_value(form_value: &'v RawStr) -> Result<Self, Self::Error> {
    let sha512_regex = Regex::new("^[0-9A-Fa-f]{128}$").unwrap();
    let hash = form_value.as_str();

    if sha512_regex.is_match(hash) {
      Ok(Sha512Hash(hash.to_string()))
    } else {
      Err(form_value)
    }
  }
}

/// Create a new Prooph.
#[get("/prooph?<hash>")]
fn new_prooph(hash: Sha512Hash, sk: State<SecretKey>) -> String {
  prooph::sign(hash.0, sk.inner()).unwrap()
}

pub fn rocket() -> rocket::Rocket {
  let sk = prooph::generate().unwrap();

  rocket::ignite()
    .mount(
      "/",
      StaticFiles::from(concat!(env!("CARGO_MANIFEST_DIR"), "/static")),
    )
    .mount("/", routes![new_prooph])
    .manage(sk)
}

fn main() {
  rocket().launch();
}
