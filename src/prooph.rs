extern crate minisign;

use minisign::{KeyPair, SecretKey};

use std::fs::File;
use std::io::Cursor;
use std::path::Path;

/// Reads or generates a signing-capable key.
pub fn generate() -> minisign::Result<SecretKey> {
  let sk_path = concat!(env!("CARGO_MANIFEST_DIR"), "/prooph.key");
  let password = ""; // TODO: be able to change password

  match SecretKey::from_file(Path::new(sk_path), Some(password.to_string())) {
    Ok(sk) => Ok(sk),
    Err(_) => {
      let pk_writer = File::create(concat!(env!("CARGO_MANIFEST_DIR"), "/static/prooph.pub"))?;
      let sk_writer = File::create(sk_path)?;
      let KeyPair { pk: _, sk } = KeyPair::generate_and_write_encrypted_keypair(
        pk_writer,
        sk_writer,
        Some("Prooph Server"),
        Some(password.to_string()),
      )
      .unwrap();

      Ok(sk)
    }
  }
}

/// Signs the given message.
pub fn sign(plaintext: String, sk: &SecretKey) -> minisign::Result<String> {
  let data_reader = Cursor::new(plaintext);
  let signature_box = minisign::sign(None, &sk, data_reader, false, None, None).unwrap();

  Ok(signature_box.into_string())
}
