# Prooph

A simple service to prove a piece of information exists at a certain time.

## Features
Simply exists on a server with some GPG keys and a text file with the current date and sha256 sum.

It provides a stupid simple third-party service that can be reasonably trusted for anything unimportant.